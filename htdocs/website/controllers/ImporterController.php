<?php

class ImporterController extends Website_Controller_Action {

    public function defaultAction () {

    }
    public function defaultEanAction () {

    }

    public function processCsvAction () {
        var_dump("process csv");

        try {

            // Undefined | Multiple Files | $_FILES Corruption Attack
            // If this request falls under any of them, treat it invalid.
            if (
                !isset($_FILES['updatedProducts']['error']) ||
                is_array($_FILES['updatedProducts']['error'])
            ) {
                throw new RuntimeException('Invalid parameters.');
            }

            // Check $_FILES['updatedProducts']['error'] value.
            switch ($_FILES['updatedProducts']['error']) {
                case UPLOAD_ERR_OK:
                    break;
                case UPLOAD_ERR_NO_FILE:
                    throw new RuntimeException('No file sent.');
                case UPLOAD_ERR_INI_SIZE:
                case UPLOAD_ERR_FORM_SIZE:
                    throw new RuntimeException('Exceeded filesize limit.');
                default:
                    throw new RuntimeException('Unknown errors.');
            }


            // is it csv file? we still need to check that the breaker is ; and not ,
            if ($_FILES["file"]["type"] == "text/csv") {
                throw new RuntimeException('Invalid file format.');
            }

            $csvData = file_get_contents($_FILES['updatedProducts']['tmp_name']);
            $lines = explode(PHP_EOL, $csvData);
            $array = array();
            foreach ($lines as $line) {
                $array[] = str_getcsv($line, $delimiter = ";");
            }
            $headers = array_shift($array);
            if($headers[0] !== "Product Type"
                || $headers[1] !== "GTIN"
                || $headers[2] !== "Product-ID"
                || $headers[3] !== "Brand"
                || $headers[4] !== "Regulated Product Name"
                || $headers[5] !== "Start availability date"
                || $headers[6] !==  "Ingredients List"
                || $headers[7] !== "Energie-kJ/100g"
                || $headers[8] !== "Energie-Kcal /100"
                || $headers[9] !== "Fett/100"
                || $headers[10] !== "davon gesättigte Fettsaeuren/100"
                || $headers[11] !==  "Kohlenhydrate/100"
                || $headers[12] !==  "davon Zucker/100g"
                || $headers[13] !== "Eiweiß/100"
                || $headers[14] !== "Salz/100"
                || $headers[15] !== "Ballaststoffe/100"
            ){
                throw new RuntimeException('Invalid content');
            };



             $this->view->precheckResult = "File is uploaded successfully.";



            $items = Object_Product::getList(array(
                "orderKey" => "productTitle",
                "order" => "desc"
            ));


            foreach($array as $product){

                foreach($items as $item){
                    $output = '';
                    $outputLang = '';
                    $trimmedEan = substr($product[1], 1, -2);
                    $language= substr($product[1], -2);

                    $languageTiny= "";
                    if($language == "DE") {
                        //update
                        $outputLang .= 'DE <br>';
                        $languageTiny="de";


                    }else if($language == "EN"){
                        //in english
                        $outputLang .= 'EN <br>';
                        $languageTiny="en";


                    }else{
                        $output .= 'incorrect language';
                    }

                    if($item->getEan() ===  $trimmedEan){
                        if($item->getTextIngredients($languageTiny) !== $product[6]){
                            $item->setTextIngredients($product[6], $languageTiny);
                            $output .='Ingredients </br>';
                        }

                        if($item->getProductTitle($languageTiny) !== $product[4]){
                            $item->setProductTitle($product[4], $languageTiny);
                            $output .='Product name</br>';
                        }
                        if(    $item-> getNutritionalTable()->getCalories()["hundred"] !== $product[7]
                            || $item-> getNutritionalTable()->getCalorieskj()["hundred"] !== $product[8]
                            || $item-> getNutritionalTable()->getProtein()["hundred"] !== $product[13]
                            || $item-> getNutritionalTable()->getCarbohydrates()["hundred"] !== $product[11]
                            || $item-> getNutritionalTable()->getSugar()["hundred"] !== $product[12]
                            || $item-> getNutritionalTable()->getFat()["hundred"] !== $product[9]
                            || $item-> getNutritionalTable()->getFattyacids()["hundred"] !== $product[10]
                            || $item-> getNutritionalTable()->getRoughage()["hundred"] !== $product[15]
                            || $item-> getNutritionalTable()->getSodium()["hundred"] !== $product[14]){
                            $table = $item->getNutritionalTable();
                            $arrayTemp =
                                array(
                                    "calories" => array("hundred" => $product[7], "portion" => NULL, "gda" => NULL),
                                    "calorieskj" => array("hundred" => $product[8], "portion" => NULL, "gda" => NULL),
                                    "protein" => array("hundred" => $product[13], "portion" => NULL, "gda" => NULL),
                                    "carbohydrates" => array("hundred" => $product[11], "portion" => NULL, "gda" => NULL),
                                    "sugar" => array("hundred" => $product[12], "portion" => NULL, "gda" => NULL),
                                    "fat" => array("hundred" => $product[9], "portion" => NULL, "gda" => NULL),
                                    "fattyacids" => array("hundred" => $product[10], "portion" => NULL, "gda" => NULL),
                                    "roughage" => array("hundred" => $product[15], "portion" => NULL, "gda" => NULL),
                                    "sodium" => array("hundred" => $product[14], "portion" => NULL, "gda" => NULL)
                                );
                            $table-> setData($arrayTemp) ;
                            $output .='Nutritional Table</br>';
                        }
                        if(strlen($output) >= 1){
                            $changeLog =  ' </br> Time: ' .date('Y-m-d h:s') .'</br>'.
                                'ProductId: ' . $item->getId() .'</br>'.
                                'Product Ean: ' . $item->getEan() .'</br>'.
                                $outputLang .'</br>'.
                                'The updated fields in product  '.$item->getEan(). ' were: </br> '. $output .'</br>';
                        }else{
                            $changeLog =  ' </br> Time: ' .date('Y-m-d H:i:s') .'</br>'.
                                'ProductId: ' . $item->getId() .'</br>'.
                                'Product Ean: ' . $item->getEan() .'</br>'.
                                'The product is up to date'.'</br>'.'</br>';
                        }
                        $item->save();
                        $this->view->resultsText .= $changeLog;
                    }


/*
                    if($item->getEan() ===  $trimmedEan){



                        if(strlen($output) >= 1){
                            $changeLog =  ' </br> Time: ' .date('Y-m-d h:s') .'</br>'.
                                'ProductId: ' . $item->getId() .'</br>'.
                                'Product Ean: ' . $item->getEan() .'</br>'.
                                'The updated fields in product  '.$item->getEan(). ' were: </br> '. $output .'</br>';
                        }else{
                            $changeLog =  ' </br> Time: ' .date('Y-m-d H:i:s') .'</br>'.
                                'ProductId: ' . $item->getId() .'</br>'.
                                'Product Ean: ' . $item->getEan() .'</br>'.
                                'The product is up to date'.'</br>'.'</br>';
                        }
                        $item->save();
                        $this->view->resultsText .= $changeLog;

                    }*/
                }
            }

        } catch (RuntimeException $e) {
            $this->view->precheckResultError = $e->getMessage();
        }
    }


    public function fixEanNumbersAction () {
        try {

            // Undefined | Multiple Files | $_FILES Corruption Attack
            // If this request falls under any of them, treat it invalid.
            if (
                !isset($_FILES['updatedProducts']['error']) ||
                is_array($_FILES['updatedProducts']['error'])
            ) {
                throw new RuntimeException('Invalid parameters.');
            }

            // Check $_FILES['updatedProducts']['error'] value.
            switch ($_FILES['updatedProducts']['error']) {
                case UPLOAD_ERR_OK:
                    break;
                case UPLOAD_ERR_NO_FILE:
                    throw new RuntimeException('No file sent.');
                case UPLOAD_ERR_INI_SIZE:
                case UPLOAD_ERR_FORM_SIZE:
                    throw new RuntimeException('Exceeded filesize limit.');
                default:
                    throw new RuntimeException('Unknown errors.');
            }


            // is it csv file? we still need to check that the breaker is ; and not ,
            if ($_FILES["file"]["type"] == "text/csv") {
                throw new RuntimeException('Invalid file format.');
            }

            $csvData = file_get_contents($_FILES['updatedProducts']['tmp_name']);
            $lines = explode(PHP_EOL, $csvData);
            $array = array();
            foreach ($lines as $line) {
                $array[] = str_getcsv($line, $delimiter = ";");
            }
            $headers = array_shift($array);




            $this->view->precheckResult = "File is uploaded successfully.";



            $items = Object_Product::getList(array(
                "orderKey" => "productTitle",
                "order" => "desc"
            ));


            foreach($array as $product){

                foreach($items as $item){
                    $output = '';
                    if($item->getEan() ===  $product[1]){
                            $item->setEan($product[23]);
                            $output .='Ingredients </br>';




                        if(strlen($output) >= 1){
                            $changeLog =  ' </br> Time: ' .date('Y-m-d h:s') .'</br>'.
                                'ProductId: ' . $item->getId() .'</br>'.
                                'Product Ean: ' . $item->getEan() .'</br>'.
                                'The updated fields in product  '.$item->getEan(). ' were: </br> '. $output .'</br>';
                        }else{
                            $changeLog =  ' </br> Time: ' .date('Y-m-d H:i:s') .'</br>'.
                                'ProductId: ' . $item->getId() .'</br>'.
                                'Product Ean: ' . $item->getEan() .'</br>'.
                                'The product is up to date'.'</br>'.'</br>';
                        }
                        $item->save();
                        $this->view->resultsText .= $changeLog;

                    }
                }
            }

        } catch (RuntimeException $e) {
            $this->view->precheckResultError = $e->getMessage();
        }
    }

}
