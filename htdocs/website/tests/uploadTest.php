<?php
class UploadTest extends PHPUnit_Framework_TestCase
{

    /**
     * @covers Upload::receive
     */
    public function testReceive()
    {
        $this->assertTrue($this->_object->receive('test'));
    }

}